import sys
import pandas as pd

# Verifica que se hayan proporcionado suficientes argumentos
if len(sys.argv) < 3:
    print("Uso: python magento-additional_attributes.py input_file.csv output_file.xlsx")
    sys.exit(1)

# Obtén los nombres de archivo de entrada y salida desde los argumentos de la línea de comandos
input_file = sys.argv[1]
output_file = sys.argv[2]

# Lee el archivo CSV
df = pd.read_csv(input_file)

# Divide la columna 'additional_attributes' en atributos individuales
df['additional_attributes'] = df['additional_attributes'].fillna('')
attributes_list = df['additional_attributes'].str.split(',')

# Inicializa un nuevo DataFrame para el archivo de Excel
excel_df = pd.DataFrame(columns=['sku'])

# Itera sobre la lista de atributos y agrega columnas al nuevo DataFrame
for index, attributes in enumerate(attributes_list):
    row_data = {'sku': df.at[index, 'sku']}
    for attribute in attributes:
        key_value = attribute.split('=')
        if len(key_value) == 2:
            key, value = key_value
            if key not in excel_df.columns:
                excel_df[key] = ''
            row_data[key] = value
    excel_df = excel_df.append(row_data, ignore_index=True)

# Rellena los valores vacíos con la cadena "VACIO"
excel_df = excel_df.fillna('VACIO')

# Guarda el resultado en un archivo Excel
excel_df.to_excel(output_file, index=False)
